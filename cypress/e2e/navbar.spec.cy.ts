describe('Utilizar la barra de busqueda de productos', () => {
  it('Validar que al enviar caracteres extranos solo reciba caracteres normales', () => {
    cy.visit('/angular');
    cy.get('[type="search"]').type('+a+c+b').should('have.value', 'acb');
  });
});