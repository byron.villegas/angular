describe('Entrar en la pagina principal', () => {
  it('Validar que el footer tenga el seteado el anio actual', () => {
    const year = (new Date).getFullYear();
    cy.visit('/angular');
    cy.get('[id="year"]').should('have.text', '© ' + year);
  });
});