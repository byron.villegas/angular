describe('Entrar en la pagina principal', () => {
  it('Validar que el titulo sea Angular', () => {
    cy.visit('/angular');
    cy.title().should('eq', 'Angular');
  });
});