import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FooterComponent } from './footer.component';

describe('Testeo de Footer Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        FooterComponent
      ],
    }).compileComponents();
  });

  it('Deberia crear el Footer Component', () => {
    const fixture = TestBed.createComponent(FooterComponent);

    const component = fixture.componentInstance;
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});