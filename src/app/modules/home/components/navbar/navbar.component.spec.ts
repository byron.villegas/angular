import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NavbarComponent } from './navbar.component';

@NgModule()
export class FixNavigationTriggeredOutsideAngularZoneNgModule {
  constructor(_router: Router) {
  }
}


describe('Testeo de Navbar Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        FixNavigationTriggeredOutsideAngularZoneNgModule
      ],
      declarations: [
        NavbarComponent
      ],
    }).compileComponents();
  });

  it('Deberia crear el Navbar Component', () => {
    const fixture = TestBed.createComponent(NavbarComponent);

    const component = fixture.componentInstance;

    component.ngOnInit();
    component.form;
    component.form.setValue({['text']: 'a'});
    component.onSearch();
    
    expect(component).toBeTruthy();
  });
});