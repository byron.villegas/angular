import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialCharactersDirective } from './directives/special-characters.directive';
import { PricePipe } from './pipes/price.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SpecialCharactersDirective, PricePipe],
  exports: [SpecialCharactersDirective, PricePipe],
  providers: [SpecialCharactersDirective, PricePipe]
})
export class SharedModule { }