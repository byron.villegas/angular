import { Pipe, PipeTransform } from '@angular/core';
import { Regex } from 'src/app/core/constants/regex';
import { Symbol } from 'src/app/core/constants/symbol';

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

  transform(value: number): string {
    return `${Symbol.DOLLAR} ${value.toString().replace(Regex.PRICE_REPLACE, Symbol.DOT)}`;
  }
}