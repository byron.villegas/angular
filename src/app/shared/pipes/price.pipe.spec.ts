import { PricePipe } from "./price.pipe";

describe('Testeo de Price Pipe', () => {
    const pipe = new PricePipe();

    it('Transforma de 1000 a $ 1.000', () => {
        expect(pipe.transform(1000)).toBe('$ 1.000');
    });
});