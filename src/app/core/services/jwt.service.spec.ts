import { JwtService } from "./jwt.service";

describe('Testeo de JwtService', () => {
    let service: JwtService;

    beforeEach(() => { service = new JwtService(); });

    it('Deberia venir el valor en nulo', () => {
        service.destroyToken();
        expect(service.getToken()).toBeNull();
    });
    it('Deberia setear el token en el almacenamiento de la sesion y posteriormente obtener el token con el mismo valor', () => {
        const value = 'abc';
        service.saveToken(value);
        expect(service.getToken()).toBe(value);
    });
    it('Deberia destruir el token del almacenamiento de la sesion y posteriormente obtener el token con valor nulo', () => {
        service.destroyToken();
        expect(service.getToken()).toBeNull();
    });
});