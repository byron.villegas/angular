import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  readonly sessionStorageKey: string = 'jwtToken';

  constructor() { }

  getToken(): string | null {
    return sessionStorage.getItem(this.sessionStorageKey);
  }

  saveToken(token: string): void {
    sessionStorage.setItem(this.sessionStorageKey, token);
  }

  destroyToken() {
    sessionStorage.removeItem(this.sessionStorageKey);
  }
}