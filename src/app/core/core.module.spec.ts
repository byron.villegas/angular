import { TestBed } from '@angular/core/testing';
import { CoreModule } from './core.module';

describe('Testeo de Core Module', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
    });
  });

  it('Deberia crear el Core Module', () => {
    const module = TestBed.inject(CoreModule);
    expect(module).toBeTruthy();
  });
});