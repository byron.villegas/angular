import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtService } from '../services/jwt.service';
import { HttpHeaders } from '../constants/http-headers';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
    readonly tokenType: string = 'Bearer';

    constructor(private jwtService: JwtService) { }

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.jwtService.getToken();
        if (token) {
            const request = httpRequest.clone({
                headers: httpRequest.headers.append(HttpHeaders.AUTHORIZATION, `${this.tokenType} ${token}`)
            });
            return next.handle(request);
        }
        return next.handle(httpRequest);
    }
}