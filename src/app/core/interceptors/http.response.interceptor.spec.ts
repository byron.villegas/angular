import { HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from "@angular/core/testing";
import { HttpStatus } from "../constants/http-status";
import { JwtService } from "../services/jwt.service";
import { HttpResponseInterceptor } from "./http.response.interceptor";

describe('Testeo de Http Response Interceptor', () => {
    let client: HttpClient;
    let controller: HttpTestingController;
    let service: JwtService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: HttpResponseInterceptor,
                    multi: true
                }
            ],
        });

        client = TestBed.inject(HttpClient);
        controller = TestBed.inject(HttpTestingController);
        service = new JwtService();
    });

    describe('Si el servicio responde que no posee internet', () => {
        beforeEach(() => {
            spyOnProperty(Navigator.prototype, 'onLine').and.returnValue(false);
        });
        it('Se imprime en consola que no esta conectado a internet', (done) => {
            client.get('/notInternet').subscribe(response => {
                expect(window.navigator.onLine).toBeFalse();
                done();
            });

            const testRequest = controller.expectOne('/notInternet');
            testRequest.flush(null, { status: 200, statusText: 'Ok'});
        });
    });

    describe('Si el servicio responde 401 unauthorized', () => {
        beforeEach(() => {
            service.saveToken('abc');
        });
        it('Se destruye el token del session storage', (done) => {
            client.get('/unAuthorized').subscribe(response => {

            },
            error => {
                expect(error.status).toBe(HttpStatus.UNAUTHORIZED)
                expect(service.getToken()).toBeNull();
                done();
            });

            const testRequest = controller.expectOne('/unAuthorized');
            testRequest.flush(null, { status: 401, statusText: 'Unauthorized' });
        });
    });
    describe('Si el servicio responde 400 bad request', () => {
        it('Se imprime en consola el request con el body malo', (done) => {
            const body = { example: true }
            client.post('/badRequest', body).subscribe(response => {

            },
            error => {
                expect(error.status).toBe(HttpStatus.BAD_REQUEST);
                done();
            });

            const testRequest = controller.expectOne('/badRequest');
            testRequest.flush(body, { status: 400, statusText: 'Bad Request' });
        });
    });
    describe('Si el servicio responde 500 internal server error', () => {
        it('Se imprime en consola el http status', (done) => {
            client.get('/internalServerError').subscribe(response => {

            },
            error => {
                expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
                done();
            });

            const testRequest = controller.expectOne('/internalServerError');
            testRequest.flush(null, { status: 500, statusText: 'Internal Server Error' });
        });
    });
});