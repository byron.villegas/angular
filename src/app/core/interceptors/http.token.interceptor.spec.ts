import { HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from "@angular/core/testing";
import { JwtService } from "../services/jwt.service";
import { HttpTokenInterceptor } from "./http.token.interceptor";

describe('Testeo de Http Token Interceptor', () => {
    let client: HttpClient;
    let controller: HttpTestingController;
    let service: JwtService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: HttpTokenInterceptor,
                    multi: true
                }
            ],
        });

        client = TestBed.inject(HttpClient);
        controller = TestBed.inject(HttpTestingController);
        service = new JwtService();
    });

    describe('Si posee token', () => {
        beforeEach(() => {
            service.saveToken('abc');
        });
        it('Se agrega el token en el header del request', (done) => {
            const body = { example: true }
            client.get('/withToken').subscribe(response => {
                expect(testRequest.request.headers.has('Authorization')).toBeTrue();
                done();
            });

            const testRequest = controller.expectOne('/withToken');
            testRequest.flush(body); // lo devuelve al done
        });
    });
    describe('Si no posee token', () => {
        beforeEach(() => {
            service.destroyToken();
        });
        it('Se ejecuta el request con los headers originales', (done) => {
            const body = { example: true }
            client.get('/withoutToken').subscribe(response => {
                expect(testRequest.request.headers.has('Authorization')).toBeFalse();
                done();
            });

            const testRequest = controller.expectOne('/withoutToken');
            testRequest.flush(body); // lo devuelve al done
        });
    });
});