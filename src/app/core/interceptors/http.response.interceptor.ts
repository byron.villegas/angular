import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpStatus } from '../constants/http-status';
import { JwtService } from '../services/jwt.service';

@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor {

    constructor(private jwtService: JwtService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            map(resp => {
                if (resp instanceof HttpResponse) {
                    if (!navigator.onLine) {
                        console.log('Tu equipo no esta conectado a Internet');
                    }
                }
                return resp;
            }),
            catchError((error: HttpErrorResponse) => {
                switch (error.status) {
                    case HttpStatus.UNAUTHORIZED:
                        console.error('Token destruido');
                        this.jwtService.destroyToken();
                        break;
                    case HttpStatus.BAD_REQUEST:
                        console.error('Error en el request', req.body);
                        break;
                    default:
                        console.error(`Error del servicio http status ${error.status}`);
                        break;
                }
                return throwError(error);
            }));
    }
}