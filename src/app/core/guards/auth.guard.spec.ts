import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { JwtService } from '../services/jwt.service';

describe('Testeo de Auth Guard', () => {

    const dummyRoute = {} as ActivatedRouteSnapshot;
    const url = '/';

    let guard: AuthGuard;
    let routerSpy: jasmine.SpyObj<Router>;
    let jwtServiceStub: Partial<JwtService>;

    beforeEach(() => {
        routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
        jwtServiceStub = {};
        guard = new AuthGuard(routerSpy, jwtServiceStub as JwtService);
    });

    describe('Cuando el usuario esta logeado', () => {
        beforeEach(() => {
            jwtServiceStub.getToken = () => { return 'a' }
        });
        it('Tiene acceso a la ruta especifica', () => {
            const canActivate = guard.canActivate(dummyRoute, fakeRouterState(url));

            expect(canActivate).toBeTrue();
        });
        it('Tiene acceso a las rutas hijas de una ruta', () => {
            const canActivateChild = guard.canActivateChild(dummyRoute, fakeRouterState(url));

            expect(canActivateChild).toBeTrue();
        });
    });

    describe('Cuando el usuario se deslogea', () => {
        beforeEach(() => {
            jwtServiceStub.getToken = () => { return null }
        });
        it('Pierde el acceso a la ruta especifica', () => {
            const notCanActivate = guard.canActivate(dummyRoute, fakeRouterState(url));

            expect(notCanActivate).toBeFalse();
        });
        it('Pierde el acceso a las rutas hijas de una ruta', () => {
            const notCanActivateChild = guard.canActivateChild(dummyRoute, fakeRouterState(url));

            expect(notCanActivateChild).toBeFalse();
        });
    });
});

function fakeRouterState(url: string): RouterStateSnapshot {
    return { url } as RouterStateSnapshot;
}