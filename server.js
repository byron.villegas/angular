const express = require('express');
const path = require('path');

const app = express();

app.use('/angular/', express.static(__dirname + '/dist')); // Para la ruta genera archivos estaticos

app.get('/angular/', (req, res) => { // Al entrar en la ruta base
    res.sendFile(path.join(__dirname + '/dist/index.html'));  // Retorna el index.html
});

app.listen(process.env.PORT || 4200);