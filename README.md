# Angular

Proyecto Angular + Express + Test Unitarios (karma + jasmin) + Test End to End (cypress)

[**Vista Previa**](https://angular-i4gn.onrender.com/angular/#/)

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

**Clonar mediante SSH**
```shell
git clone git@gitlab.com:byron.villegas/angular.git
```
**Clonar mediante HTTPS**
```shell
git clone https://gitlab.com/byron.villegas/angular.git
```

Mira Deployment para conocer como desplegar el proyecto.

### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

| Software | Versión  |
|----------|----------|
| node     | v16.17.0 |
| npm      | 8.15.0    |

#### Instalar Node

Para instalar Node debemos ir a la siguiente pagina: https://nodejs.org/en/download/ descargar el instalador, ejecutarlo y seguir los pasos para la instalación.

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

Instalar las dependencias declaradas en el **package.json** mediante el siguiente comando:

```shell
npm install
```
**NOTA:** Node instalara todas las depedencias necesarias incluyendo las de desarrollo (test unitarios, test end to end, etc).

Instalación de dependencias finalizada mostrando el siguiente resultado en consola:

```shell
added 1126 packages, and audited 1127 packages in 43s

141 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

_Finaliza con un ejemplo de cómo obtener datos del sistema o como usarlos para una pequeña demo_

Para desplegar la aplicación tenemos las siguientes formas:

Por defecto:

```shell
ng serve
```
**NOTA:** La aplicación se iniciará (cualquier cambio realizado en un archivo hará que la aplicación se refresque automáticamente).

Con Express:

```shell
npm start
```
**NOTA:** Si se realiza un cambio a la aplicación no se reiniciará automáticamente.

La aplicación se desplegará exitosamente mostrando el siguiente resultado en consola:

Ng Serve: 
```shell
✔ Browser application bundle generation complete.

Initial Chunk Files   | Names         |  Raw Size
vendor.js             | vendor        |   2.33 MB | 
styles.css, styles.js | styles        | 397.88 kB | 
polyfills.js          | polyfills     | 313.07 kB | 
scripts.js            | scripts       | 146.14 kB | 
main.js               | main          |  27.43 kB | 
runtime.js            | runtime       |   6.51 kB | 

                      | Initial Total |   3.20 MB

Build at: 2022-08-19T22:35:09.536Z - Hash: 17719c1ed5c0e41e - Time: 4951ms

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/angular **


√ Compiled successfully.
```

Express:
```shell
> angular@1.0.0 start
> node server.js
```

## Ejecutando las pruebas ⚙️

_Explica como ejecutar las pruebas automatizadas para este sistema_

### Pruebas unitarias 📑

_Explica que verifican estas pruebas y por qué_

Los test unitarios son para comprobar que un fragmento de código determinado está funcionando de manera correcta, cabe destacar que si modificamos una funcionalidad toda prueba unitaria asociada a esa funcionalidad fallará si no es refactorizada debidamente.

#### Configuración

##### karma

_Es un test runner, desarrollado por el equipo de angular, que nos permite automatizar algunas tareas de los frames de tests, como jasmine._

Para configurar karma utilizaremos el siguiente archivo:

###### karma.conf.js

```javascript
module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('@chiragrupani/karma-chromium-edge-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-sabarivka-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      jasmine: {},
      clearContext: false
    },
    jasmineHtmlReporter: {
      suppressAll: true
    },
    coverageReporter: {
      dir: require('path').join(__dirname, './coverage/'),
      subdir: '.',
      include: [
        'src/**/*.(ts|js)',
        '!src/**/*.spec.*',
        '!src/**/*main.ts',
        '!src/**/*test.ts',
        '!src/**/*environment.**'
      ],
      reporters: [
        { type: 'html' },
        { type: 'text-summary' }
      ],
      check: {
        global: {
          statements: 90,
          branches: 90,
          functions: 90,
          lines: 90
        }
      }
    },
    reporters: ['progress', 'kjhtml', 'sabarivka'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless', 'EdgeHeadless'],
    customLaunchers: {
      headless: {
        base: 'ChromeHeadless',
        flags: [
          '--no-sandbox',
          '--disable-setuid-sandbox'
        ]
      }
    },
    singleRun: true,
    restartOnFileChange: true
  });
};
```

###### Parametros

- basePath: Es la ruta base que se usará para resolver todas las rutas relativas definidas en los archivos y exclusiones.
- frameworks: Lista de frameworks para los test
- plugins: Lista de plugins a cargar (en nuestro caso utilizaremos para cambiar el navegador predeterminado de karma)
- client: Cliente a utilizar con sus respectivas configuraciones
    - clearContext: Flag para indicar si karma borra la ventana del contexto al finalizar la ejecución de las pruebas
- jasmineHtmlReporter: Configuraciones para el reporte html de jasmine
    - suppressAll: Flag para indicar si se borran todos los mensajes
- coverageReporter: Configuraciones para el reporte de cobertura
    - dir: Directorio oficial para la generación del reporte
    - subdir: Sub directorio oficial para la generacion del reporte (si es . lo deja en la ruta definida en dir)
    - include: Lista de tipos de archivos a incluir.
    - reporters: Lista de tipos de reportes
    - check: Configuraciones para validaciones referentes a las coberturas
        - branches: Porcentaje (%) a utilizar para verificar si branches cumplen o no (checkCoverage)
        - functions: Porcentaje (%) a utilizar para verificar si functions cumplen o no (checkCoverage)
        - sines: Porcentaje (%) a utilizar para verificar si lines cumplen o no (checkCoverage)
        - statements: Porcentaje (%) a utilizar para verificar si statements cumplen o no (checkCoverage)
- reporters: Lista de tipos de reportes (cabe destacar que dependiendo del tipo de reporte puede que genere la cobertura en base a todos los archivos o solo los tests)
- port: Puerto a utilizar para la ejecución en navegadores
- colors: Flag para indicar si habilita el uso de colores para el logeo
- logLevel: Nivel de log a utilizar
- autoWatch: Flag para indicar si habilita la escucha de cambios en archivos para re ejecutar las pruebas
- browsers: Lista de navegadores a utilizar (las versiones **Headless** permiten la ejecución de pruebas en segundo plano)
- customLaunchers: Configuracion utilizada para definir lanzadores personalizados (navegadores)
    - headless: Nombre del lanzador
    - base: Navegador base
    - flags: Lista de opciones a ejecutar en conjunto con el lanzador (navegador)
- singleRun: Flag para indicar si se ejecutan todos los tests en una única instancia
- restartOnFileChange: Flag para indicar si se re ejecutan las pruebas si hay algún cambio en un archivo


#### Ejecución

Para ejecutar los test unitarios debemos utilizar el siguiente comando:

```shell
ng test
```
**NOTA:** Se ejecutarán todos los tests declarados en la propiedad **include** del archivo **tsconfig.spec.json**

Los tests unitarios se ejecutarán exitosamente mostrando el siguiente resultado en consola:

```shell
✔ Browser application bundle generation complete.
19 08 2022 18:31:07.793:INFO [karma-server]: Karma v6.4.0 server started at http://localhost:9876/
19 08 2022 18:31:07.796:INFO [launcher]: Launching browsers ChromeHeadless, EdgeHeadless with concurrency unlimited
19 08 2022 18:31:07.800:INFO [launcher]: Starting browser ChromeHeadless
19 08 2022 18:31:07.807:ERROR [launcher]: Cannot start ChromeHeadless
        Can not find the binary C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
        Please set env variable CHROME_BIN
19 08 2022 18:31:07.808:ERROR [launcher]: ChromeHeadless stdout:
19 08 2022 18:31:07.809:ERROR [launcher]: ChromeHeadless stderr:
19 08 2022 18:31:07.813:INFO [launcher]: Starting browser Edge Headless
19 08 2022 18:31:08.543:INFO [Edge 104.0.1293.54 (Windows 10)]: Connected on socket iR_Yw-9AltbB9sXaAAAB with id 55946896
ERROR: 'Token destruido'
Edge 104.0.1293.54 (Windows 10): Executed 4 of 23 SUCCESS (0 secs / 0.066 secs)
ERROR: 'Error en el request', Object{example: true}                                                                                                                                                                              
Edge 104.0.1293.54 (Windows 10): Executed 5 of 23 SUCCESS (0 secs / 0.075 secs)
LOG: 'Tu equipo no esta conectado a Internet'
Edge 104.0.1293.54 (Windows 10): Executed 6 of 23 SUCCESS (0 secs / 0.078 secs)
LOG: 'Response: null'                                                                                                                                                                                                            
Edge 104.0.1293.54 (Windows 10): Executed 6 of 23 SUCCESS (0 secs / 0.078 secs)
ERROR: 'Error del servicio http status 500'                                                                                                                                                                                      
Edge 104.0.1293.54 (Windows 10): Executed 7 of 23 SUCCESS (0 secs / 0.081 secs)
ERROR: 'NG0303: Can't bind to 'formGroup' since it isn't a known property of 'form' (used in the 'NavbarComponent' component template).
1. If 'form' is an Angular component and it has the 'formGroup' input, then verify that it is a part of an @NgModule where this component is declared.
2. To allow any property add 'NO_ERRORS_SCHEMA' to the '@NgModule.schemas' of this component.'
Edge 104.0.1293.54 (Windows 10): Executed 10 of 23 SUCCESS (0 secs / 0.091 secs)
ERROR: 'NG0303: Can't bind to 'formGroup' since it isn't a known property of 'form' (used in the 'NavbarComponent' component template).
1. If 'form' is an Angular component and it has the 'formGroup' input, then verify that it is a part of an @NgModule where this component is declared.
ERROR: 'NG0303: Can't bind to 'formGroup' since it isn't a known property of 'form' (used in the 'NavbarComponent' component template).                                                                                          
1. If 'form' is an Angular component and it has the 'formGroup' input, then verify that it is a part of an @NgModule where this component is declared.
2. To allow any property add 'NO_ERRORS_SCHEMA' to the '@NgModule.schemas' of this component.'
Edge 104.0.1293.54 (Windows 10): Executed 11 of 23 SUCCESS (0 secs / 0.105 secs)
ERROR: 'NG0303: Can't bind to 'formGroup' since it isn't a known property of 'form' (used in the 'NavbarComponent' component template).
1. If 'form' is an Angular component and it has the 'formGroup' input, then verify that it is a part of an @NgModule where this component is declared.
ERROR: 'NG0304: 'app-home-navbar' is not a known element (used in the 'AppComponent' component template):
1. If 'app-home-navbar' is an Angular component, then verify that it is a part of an @NgModule where this component is declared.
2. If 'app-home-navbar' is a Web Component then add 'CUSTOM_ELEMENTS_SCHEMA' to the '@NgModule.schemas' of this component to suppress this message.'
Edge 104.0.1293.54 (Windows 10): Executed 14 of 23 SUCCESS (0 secs / 0.118 secs)
ERROR: 'NG0304: 'app-home-navbar' is not a known element (used in the 'AppComponent' component template):
1. If 'app-home-navbar' is an Angular component, then verify that it is a part of an @NgModule where this component is declared.
ERROR: 'NG0304: 'app-home-footer' is not a known element (used in the 'AppComponent' component template):                                                                                                                        
1. If 'app-home-footer' is an Angular component, then verify that it is a part of an @NgModule where this component is declared.
2. If 'app-home-footer' is a Web Component then add 'CUSTOM_ELEMENTS_SCHEMA' to the '@NgModule.schemas' of this component to suppress this message.'
Edge 104.0.1293.54 (Windows 10): Executed 14 of 23 SUCCESS (0 secs / 0.118 secs)
ERROR: 'NG0304: 'app-home-footer' is not a known element (used in the 'AppComponent' component template):
1. If 'app-home-footer' is an Angular component, then verify that it is a part of an @NgModule where this component is declared.
WARN: 'Navigation triggered outside Angular zone, did you forget to call 'ngZone.run()'?'
Edge 104.0.1293.54 (Windows 10): Executed 16 of 23 SUCCESS (0 secs / 0.124 secs)
Edge 104.0.1293.54 (Windows 10): Executed 23 of 23 SUCCESS (0.189 secs / 0.139 secs)
TOTAL: 23 SUCCESS

=============================== Coverage summary ===============================
Statements   : 100% ( 81/81 )
Branches     : 100% ( 8/8 )
Functions    : 100% ( 23/23 )
Lines        : 100% ( 64/64 )
================================================================================
```
**NOTA:** Como resultado de los test unitarios se mostrará por consola el resumen de cubertura y además se generará un reporte de coverage en el directorio /coverage en un archivo index.html.

##### Reporte de cobertura de los tests unitarios

![-----------------------------](img01.png)
**NOTA:** Como podemos ver estan los porcentajes correspondientes para las siguientes categorías: Statements, Branches, Functions y Lines

### Pruebas de End to End ✅

_Explica que verifican estas pruebas y por qué_

Los test end to end consiste en probar la aplicación desde el punto de vista del usuario final. en estas pruebas se prueban los flujos y procesos.


## Despliegue 📦

_Agrega notas adicionales sobre como hacer deploy_

Para desplegar la aplicación tenemos las siguientes formas:

Por defecto:

```shell
ng serve
```
**NOTA:** La aplicación se iniciará (cualquier cambio realizado en un archivo hará que la aplicación se refresque automáticamente).

Con Express:

```shell
npm start
```
**NOTA:** Si se realiza un cambio a la aplicación no se reiniciará automáticamente.

La aplicación se desplegará exitosamente mostrando el siguiente resultado en consola:

Ng Serve: 
```shell
✔ Browser application bundle generation complete.

Initial Chunk Files   | Names         |  Raw Size
vendor.js             | vendor        |   2.33 MB | 
styles.css, styles.js | styles        | 397.88 kB | 
polyfills.js          | polyfills     | 313.07 kB | 
scripts.js            | scripts       | 146.14 kB | 
main.js               | main          |  27.43 kB | 
runtime.js            | runtime       |   6.51 kB | 

                      | Initial Total |   3.20 MB

Build at: 2022-08-19T22:35:09.536Z - Hash: 17719c1ed5c0e41e - Time: 4951ms

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/angular **


√ Compiled successfully.
```

Express:
```shell
> angular@1.0.0 start
> node server.js
```

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

### Dependecias 🗃️

| Paquete                           | Versión | Página NPM                                      | Página Documentación                                           |
|-----------------------------------|---------|-------------------------------------------------|----------------------------------------------------------------|
| @angular/animations               | 14.1.0  | N/A                                             | N/A                                                            |
| @angular/common                   | 14.1.0  | N/A                                             | N/A                                                            |
| @angular/compiler                 | 14.1.0  | N/A                                             | N/A                                                            |
| @angular/core                     | 14.1.0  | N/A                                             | N/A                                                            |
| @angular/forms                    | 14.1.0  | N/A                                             | N/A                                                            |
| @angular/platform-browser         | 14.1.0  | N/A                                             | N/A                                                            |
| @angular/platform-browser-dynamic | 14.1.0  | N/A                                             | N/A                                                            |
| @angular/router                   | 14.1.0  | N/A                                             | N/A                                                            |
| bootstrap                         | 5.2.0   | https://www.npmjs.com/package/bootstrap         | https://getbootstrap.com/docs/5.0/getting-started/introduction |
| express                           | 4.18.1  | https://www.npmjs.com/package/express           | https://expressjs.com/en/guide/routing.html                    |
| jquery                            | 3.6.0   | https://www.npmjs.com/package/jquery            | https://api.jquery.com/                                        |
| path                              | 0.12.7  | https://www.npmjs.com/package/path              | https://nodejs.org/api/path.html                               |
| rxjs                              | 7.5.0   | N/A                                             | N/A                                                            |
| tslib                             | 2.3.0   | N/A                                             | N/A                                                            |
| zone.js                           | 0.11.4  | N/A                                             | N/A                                                            |

### Depedencias de desarrollo 🗃️

| Paquete                                    | Versión | Página NPM                                                               | Página Documentación                                       |
|--------------------------------------------|---------|--------------------------------------------------------------------------|------------------------------------------------------------|
| @angular-devkit/build-angular              | 14.1.2  | N/A                                                                      | N/A                                                        |
| @angular/cli                               | 14.1.2  | N/A                                                                      | N/A                                                        |
| @angular/compiler-cli                      | 14.1.0  | N/A                                                                      | N/A                                                        |
| @chiragrupani/karma-chromium-edge-launcher | 2.2.2   | https://www.npmjs.com/package/@chiragrupani/karma-chromium-edge-launcher | N/A                                                        |
| @cypress/schematic                         | 2.0.2   | https://www.npmjs.com/package/@cypress/schematic                         | N/A                                                        | 
| @types/jasmine                             | 4.0.0   | N/A                                                                      | N/A                                                        |
| cypress                                    | 10.6.0  | https://www.npmjs.com/package/cypress                                    | N/A                                                        |
| jasmine-core                               | 4.2.0   | N/A                                                                      | N/A                                                        |
| karma                                      | 6.4.0   | N/A                                                                      | N/A                                                        |
| karma-chrome-launcher                      | 3.1.0   | N/A                                                                      | N/A                                                        |
| karma-coverage                             | 2.2.0   | N/A                                                                      | N/A                                                        |
| karma-jasmine                              | 5.1.0   | N/A                                                                      | N/A                                                        |
| karma-jasmine-html-reporter                | 2.0.0   | N/A                                                                      | N/A                                                        |
| karma-sabarivka-reporter                   | 3.3.1   | https://www.npmjs.com/package/karma-sabarivka-reporter                   | https://github.com/kopach/karma-sabarivka-reporter         |
| mocha                                      | 10.0.0  | N/A                                                                      | N/A                                                        |
| mochawesome                                | 7.1.3   | https://www.npmjs.com/package/mochawesome                                | https://github.com/adamgruber/mochawesome                  |
| mochawesome-merge                          | 4.2.1   | https://www.npmjs.com/package/mochawesome-merge                          | https://github.com/Antontelesh/mochawesome-merge           |
| mochawesome-report-generator               | 6.2.0   | https://www.npmjs.com/package/mochawesome-report-generator               | https://github.com/adamgruber/mochawesome-report-generator |
| typescript                                 | 4.7.2   | N/A                                                                      | N/A                                                        |


## Contribuyendo 🤝

Por favor lee el [CONTRIBUTING](CONTRIBUTING.md) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://gitlab.com/byron.villegas/node-express/-/wikis/Wiki)

## Medallas 🥇

Usamos [Shields](https://shields.io/) para la generación de las medallas.

## Versionado 📌

Usamos [SemVer](https://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://gitlab.com/byron.villegas/node-express/-/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

- **Byron Villegas** - *Desarrollador* - [byron.villegas](https://gitlab.com/byron.villegas)

También puedes mirar la lista de todos los [contribuyentes](https://gitlab.com/byron.villegas/node-express/-/project_members) quíenes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (MIT) - mira el archivo [LICENSE](LICENSE) para detalles
